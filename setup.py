#!/usr/bin/python3
# SPDX-License-Identifier: MIT

from setuptools import setup

setup(name="mdbp",
      description="dpkg-buildpackage wrapper",
      packages=["mdbp"],
      package_data={"mdbp": ["build_schema.json"]},
      classifiers=[
          "Intended Audience :: Developers",
          "License :: OSI Approved :: MIT License",
          "Operating System :: POSIX :: Linux",
          "Topic :: Software Development :: Build Tools",
      ],
      install_requires=[
          "python_debian",
          "jsonschema",
          "requests",
      ],
      entry_points=dict(
          console_scripts=[
              "mdbp-debspawn=mdbp.debspawn:main",
              "mdbp-mmdebstrap=mdbp.mmdebstrap:main",
              "mdbp-pbuilder=mdbp.pbuilder:main",
              "mdbp-sbuild=mdbp.sbuild:main",
              "mdbp-ssh=mdbp.ssh:main",
              "mdbp-streamapi=mdbp.streamapi:main",
          ]
      )
)
