#!/usr/bin/python3
# SPDX-License-Identifier: MIT
"""mdbp backend using debspawn"""

import argparse
import itertools
import os
import pathlib
import subprocess
import sys

from .common import buildjson, clean_dir, compute_env, get_dsc, make_option

def main() -> None:
    """Entry point for mdbp-debspawn backend"""
    parser = argparse.ArgumentParser()
    parser.add_argument("buildjson", type=buildjson)
    args = parser.parse_args()
    build = args.buildjson

    if "sourcename" in build["input"]:
        raise ValueError("building a source package by name is not supported")
    if "bd-uninstallable-explainer" in build:
        raise ValueError("bd-uninstallable-explainer %r not supported" %
                         build["bd-uinstallable-explainer"])
    if build.get("build_path"):
        raise ValueError("build_path not supported")
    if build.get("host_architecture") not in (None,
                                              build.get("build_architecture")):
        raise ValueError("cross building is not supported")
    if build.get("lintian", {}).get("run", False) and \
       build["lintian"].get("options"):
        raise ValueError("setting lintian options is not supported")
    if build.get("network") == "disable":
        raise ValueError("disabling network is not supported")
    if build.get("hooks"):
        raise ValueError("hooks are not supported")

    env = compute_env(build)
    if build.get("build_profiles"):
        env["DEB_BUILD_PROFILES"] = " ".join(build["build_profiles"])
    enablelog = build["output"].get("log", True)

    cmd = [
        *(["sudo", "--"] if os.getuid() != 0 else ()),
        "debspawn", "build", "--no-buildlog",
        *make_option("--arch", build.get("build_architecture")),
        "--only",
        dict(
            any="arch",
            all="indep",
            binary="binary",
        )[build.get("type", "binary")],
        "--results-dir", build["output"]["directory"],
        *(["--run-lintian"] if build.get("lintian", {}).get("run", False)
          else ()),
        *itertools.chain.from_iterable(
            ("--setenv", "%s=%s" % item) for item in env.items()),
        build["distribution"],
    ]
    with get_dsc(build) as dscpath:
        ret = subprocess.call([*cmd, str(dscpath)],
                              stdout=None if enablelog else subprocess.DEVNULL,
                              stderr=subprocess.STDOUT if enablelog
                                     else subprocess.DEVNULL,
                              )
    clean_dir(pathlib.Path(build["output"]["directory"]),
              build["output"].get("artifacts", ["*"]))
    sys.exit(ret)

if __name__ == "__main__":
    main()
