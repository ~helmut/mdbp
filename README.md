mdbp
====

A machine-friendly abstraction of build environments such as `sbuild` or
`pbuilder`. Primarily, it is an API. To make it useful, it also carries a
couple of implementations henceforth called backends. Each backend is supplied
by its own command.

Usage
-----

To use it, create a JSON file describing the build you intend to perform. There
is a schema describing the available parameters in `mdbp/build_schema.json`. An
example build request could be:

    {
      "input": {"source_package_path": "./hello_2.10-2.dsc"},
      "distribution": "unstable",
      "output": {"directory": "out"}
    }

The output directory `out` is supposed to be an empty directory. Then run one
of the backends `mdbp-BACKEND [BACKENDOPTIONS] REQUEST.json`. The process
blocks until the build has finished and its return code signifies success or
failure of the operation. Usage of unsupported parameters of the particular
backend result in a failure of the operation as a whole. The content of the
stdout file descriptor is to be considered the build log.

When to use mdbp?
-----------------

The target audience performs many builds in a mechanical way. Typically, a
higher level application needs to perform Debian package builds. Multiple users
of such an application may prefer different backends.

When not to use mdbp?
---------------------

Detailed customization and interaction are non-goals. If you need to spawn an
interactive shell during a failed build, using the underlying implementation is
better suited.

mdbp-sbuild
-----------

This backend uses `sbuild` to perform the actual build. It expects that
`sbuild` is set up and the user running the build has permission to do so. It
has no backend-specific options.

mdbp-pbuilder
-------------

This backend uses `pbuilder` to perform the actual build. Unless run as root,
it performs the build via `sudo`. It assumes that suitable tarballs exist for.
For distributions `unstable` and `sid`, it'll use the predefined one and for
any other it'll look for a `<distribution>-base.tgz` or `<distribution>.tgz` in
`/var/cache/pbuilder`. It has no backend-specific options.

mdbp-mmdebstrap
---------------

This backend bootstraps a temporary chroot in a user namespace and performs the
build within. As such it requires working user namespaces including a subuid
allocation and the suid tool `newuidmap`. Unlike other backends, it does not
need a chroot or base to be set up before. Given that there is no external
state, it allows supplying the mirror to be used via the `--mirror` option.

mdbp-ssh
--------

This is a backend proxy that performs builds on remote hosts via ssh. On the
remote side, it calls a separate non-backend `mdbp-streamapi`, that allows
piping all of the relevant paramters through `stdin`, `stdout` and `stderr`.
`mdbp-streamapi` calls into another backend to perform the build. As such, you
need to pass both a host and a backend before passing the build description.
